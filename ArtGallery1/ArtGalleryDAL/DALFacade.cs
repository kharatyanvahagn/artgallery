﻿using ArtGalleryDAL.Interfaces;
using ArtGalleryDAL.UOW;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryDAL
{
    public class DALFacade
    {
         public IUnitOfWork UnitOfWork
        {
            get
            {
                return new UnitOfWork();
            }
        }
    }
}
