﻿using ArtGalleryDAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;


namespace ArtGalleryDAL.Context
{
    class ArtGalleryContext : DbContext
    {
      static DbContextOptions<ArtGalleryContext> options =
            new DbContextOptionsBuilder<ArtGalleryContext>().UseInMemoryDatabase("database_name").Options;


        //Options That we want in Memory
        /*public ArtGalleryContext() : base(options)
        {

        }*/

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
                //base.OnConfiguring(optionsBuilder);
            }
        }


        public DbSet<User> Users { get; set; }
        public DbSet<ArtObject> ArtObjects { get; set; }
    }
}

