﻿using ArtGalleryDAL.Context;
using ArtGalleryDAL.Interfaces;
using ArtGalleryDAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryDAL.UOW
{
    class UnitOfWork : IUnitOfWork
    {
        public IUserRepository UserRepository { get; internal set; }
        public IArtObjectRepository ArtObjectRepository { get; internal set; }
        private ArtGalleryContext context;
        public UnitOfWork()
        {
            context = new ArtGalleryContext();
            context.Database.EnsureCreated();
            UserRepository = new UserRepository(context);
            ArtObjectRepository = new ArtObjectRepository(context);
        }
        public int Complete()
        {
            return context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
