﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryDAL.Interfaces
{
   public interface IUnitOfWork : IDisposable
    {
        IUserRepository UserRepository { get; }
        IArtObjectRepository ArtObjectRepository { get; }
        int Complete();
    }
}
