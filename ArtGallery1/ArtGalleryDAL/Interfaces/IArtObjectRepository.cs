﻿using ArtGalleryDAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryDAL.Interfaces
{
    public interface IArtObjectRepository
    {
        //C
        ArtObject Create(ArtObject cust);
        //R
        List<ArtObject> GetAll();
        ArtObject Get(int Id);
        //U
        //No Update for Repository, It will be the task of Unit of Work
        //D
        ArtObject Delete(int Id);
    }
}
