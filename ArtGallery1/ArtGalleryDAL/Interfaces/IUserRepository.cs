﻿using ArtGalleryDAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryDAL.Interfaces
{
   public  interface IUserRepository
    {
        User Create(User cust);
      
        List<User> GetAll();
        User Get(int Id);

        User GetUser(string login, string password);
        User Delete(int Id);
    }
}
