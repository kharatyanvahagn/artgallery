﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryDAL.Entities
{
   public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Account { get; set; }
        public string LogIn { get; set; }
        public string Password { get; set; }
        public List<ArtObject> ArtObjects { get; set; }
        /*test result
         {
          "Id": 1,
          "FirstName":"jjj",
          "LastName ":"jjj",
          "Address":"jjj",
          "Account":"jjj",
          "LogIn ":"jjj",
          "Password ":"jjj"
          }


        //////
        {
	    "Id": 1,
        "Name":"jjj",
        "Artist ":"jjj",
        "Type":"Sculpture",
        "Description":"jjj",
        "Time":"1995-04-07T00:00:00",
        "Price ":"22.33",
        "User" : {
			    "Id": 1,
		        "FirstName":"jjj",
		        "LastName ":"jjj",
		        "Address":"jjj",
		        "Account":"jjj",
		        "LogIn ":"jjj",
		        "Password ":"jjj"
          }
        
}
  
           */
    }
}
