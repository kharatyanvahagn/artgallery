﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;
using Enums;

namespace ArtGalleryDAL.Entities
{

   public class ArtObject
    {
    
        public int Id { get; set; }
        public string Name { get; set; }
        public string Artist { get; set; }
        public Types Type { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; }
        public decimal Price { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        


    }
}
