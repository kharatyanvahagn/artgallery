﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ArtGalleryDAL.Context;
using ArtGalleryDAL.Entities;
using ArtGalleryDAL.Interfaces;

namespace ArtGalleryDAL.Repositories
{
    class UserRepository : IUserRepository
    {
        ArtGalleryContext _context;
        public UserRepository(ArtGalleryContext context)
        {
            _context = context;
        }

        public User Create(User user)
        {
            _context.Users.Add(user);
            return user;  
        }
        public User GetUser(string account, string password)
        {
            return _context.Users.FirstOrDefault(u => u.Account == account && u.Password == password );
        }

        public User Delete(int Id)
        {
            var user = Get(Id);
            _context.Users.Remove(user);
            return user;
        }

        public User Get(int Id)
        {
          return  _context.Users.FirstOrDefault(u => u.Id == Id);
        }

        public List<User> GetAll()
        {
            return _context.Users.ToList();
        }
    }
}
