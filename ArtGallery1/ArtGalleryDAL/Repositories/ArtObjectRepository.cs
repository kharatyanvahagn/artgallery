﻿using ArtGalleryDAL.Context;
using ArtGalleryDAL.Entities;
using ArtGalleryDAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtGalleryDAL.Repositories
{
 class ArtObjectRepository : IArtObjectRepository
    {
        ArtGalleryContext _context;
        public ArtObjectRepository(ArtGalleryContext cont )
        {
            _context = cont;
        }

        public ArtObject Create(ArtObject artObject)
        {
            _context.ArtObjects.Add(artObject);
            return artObject;
        }

        public ArtObject Delete(int Id)
        {
            var artObject = Get(Id);
            _context.ArtObjects.Remove(artObject);
            return artObject;

        }

        public ArtObject Get(int Id)
        {
            return _context.ArtObjects.Find(Id);
        }

        public List<ArtObject> GetAll()
        {
           return _context.ArtObjects.ToList();

        }
    }
}
