﻿using ArtGalleryBLL.Interfaces;
using ArtGalleryBLL.Services;
using ArtGalleryDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryBLL
{
   public class BLLFacade
    {
        public IUserService UserService
        {
            get { return new UserService(new DALFacade()); }
        }
        public IArtObjectService ArtObjectService
        {
            get { return new ArtObjectService(new DALFacade()); }
        }
    }
}
