﻿using ArtGalleryBLL.BusinessObjects;
using ArtGalleryBLL.Converters;
using ArtGalleryBLL.Interfaces;
using ArtGalleryDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtGalleryBLL.Services
{
    public class UserService : IUserService
    {
        UserConverter conv = new UserConverter();
       private DALFacade _facade;
       public UserService(DALFacade facade)
        {
            _facade = facade;
        }
        public UserBO Create(UserBO userBO)
        {
            
            using (var uow = _facade.UnitOfWork)
            {
              var user =  uow.UserRepository.Create(conv.Convert(userBO));
                uow.Complete();
                return conv.Convert(user);
            }

        }

        public UserBO Delete(int Id)
        {
            using (var uow = _facade.UnitOfWork)
            {
                var user = uow.UserRepository.Delete(Id);
                uow.Complete();
                return conv.Convert(user);
            }
        }

        public UserBO Get(int Id)
        {
            using (var uow = _facade.UnitOfWork)
            {
                var user = uow.UserRepository.Get(Id);
                return conv.Convert(user);
            }
        }
        public UserBO GetUser(string account, string password)
        {
            using (var uow = _facade.UnitOfWork)
            {
                var user = uow.UserRepository.GetUser(account,password);
                return conv.Convert(user);
            }
        }
        public List<UserBO> GetAll()
        {
            using (var uow = _facade.UnitOfWork)
            {
                return uow.UserRepository.GetAll().Select(u => conv.Convert(u)).ToList();
            }
        }

        public UserBO Update(UserBO userBO)
        {
            using (var uow = _facade.UnitOfWork)
            {
                var user = uow.UserRepository.Get(userBO.Id);
                if (user == null)
                {
                    throw new InvalidOperationException("there is no such user");
                }
                user.FirstName = userBO.FirstName;
                user.LastName = userBO.LastName;
                user.Address = user.Address;
                user.Account = userBO.Account;
                user.LogIn = userBO.LogIn;
                user.Password = userBO.Password;
                uow.Complete();
                return conv.Convert(user);
            }
        }
    }
}
