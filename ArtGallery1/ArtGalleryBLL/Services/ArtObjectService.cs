﻿using ArtGalleryBLL.BusinessObjects;
using ArtGalleryBLL.Converters;
using ArtGalleryBLL.Interfaces;
using ArtGalleryDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArtGalleryBLL.Services
{
   public class ArtObjectService : IArtObjectService
    {
        ArtObjectConverter conv = new ArtObjectConverter();
        private DALFacade _facade;
        public ArtObjectService(DALFacade facade)
        {
            _facade = facade;
        }
        public ArtObjectBO Create(ArtObjectBO cust)
        {
            using (var uow = _facade.UnitOfWork)
            {
               var artObject = uow.ArtObjectRepository.Create(conv.Convert(cust));
                uow.Complete();
                artObject.User = uow.UserRepository.Get(artObject.UserId);
                return conv.Convert(artObject);
            }
        }

        public ArtObjectBO Delete(int Id)
        {
            using (var uow = _facade.UnitOfWork)
            {
                var artObject = uow.ArtObjectRepository.Delete(Id);
                uow.Complete();
                return conv.Convert(artObject);
            }
        }

        public ArtObjectBO Get(int Id)
        {
            using (var uow = _facade.UnitOfWork)
            {
                var artObject = uow.ArtObjectRepository.Get(Id);
                artObject.User = uow.UserRepository.Get(artObject.UserId);
                return conv.Convert(artObject);
            }
        }

        public List<ArtObjectBO> GetAll()
        {
            using (var uow = _facade.UnitOfWork)
            {
              return  uow.ArtObjectRepository.GetAll().Select(art => conv.Convert(art)).ToList();
                
            }
        }

        public ArtObjectBO Update(ArtObjectBO artObjectBO)
        {
            using (var uow = _facade.UnitOfWork)
            {
                var artObject = uow.ArtObjectRepository.Get(artObjectBO.Id);
                if (artObject == null)
                {
                    throw new InvalidOperationException("there is no such artObject");
                }
                artObject.Name = artObjectBO.Name;
                artObject.Price = artObjectBO.Price;
                artObject.Time = artObjectBO.Time;
                artObject.Type = artObjectBO.Type;
                artObject.Artist = artObjectBO.Artist;
                artObject.Description = artObjectBO.Description;
                artObject.UserId = artObjectBO.UserId;
                uow.Complete();
                artObject.User = uow.UserRepository.Get(artObject.UserId);

                return conv.Convert(artObject);
            }
        }
    }
}
