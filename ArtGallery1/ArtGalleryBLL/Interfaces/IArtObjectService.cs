﻿using ArtGalleryBLL.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryBLL.Interfaces
{
   public interface IArtObjectService
    {
        //C
        ArtObjectBO Create(ArtObjectBO cust);
        //R
        List<ArtObjectBO> GetAll();
        ArtObjectBO Get(int Id);
        //U
        ArtObjectBO Update(ArtObjectBO cust);
        //D
        ArtObjectBO Delete(int Id);
    }
}
