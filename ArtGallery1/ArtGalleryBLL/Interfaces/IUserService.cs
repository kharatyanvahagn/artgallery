﻿using ArtGalleryBLL.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryBLL.Interfaces
{
    public interface IUserService
    {
        //C
        UserBO Create(UserBO cust);
        //R
        List<UserBO> GetAll();
        UserBO Get(int Id);
        //U
        UserBO Update(UserBO cust);
        //D
        UserBO Delete(int Id);
        UserBO GetUser(string login, string password);
    }
}
