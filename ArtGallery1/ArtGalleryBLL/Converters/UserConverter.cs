﻿using ArtGalleryBLL.BusinessObjects;
using ArtGalleryDAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryBLL.Converters
{
    public class UserConverter
    {
        internal User Convert(UserBO userBO)
        {
            if (userBO == null) { return null; }
            return new User()
            {
                Id = userBO.Id,
                FirstName = userBO.FirstName,
                LastName = userBO.LastName,
                Address = userBO.Address,
                Account = userBO.Account,
                LogIn = userBO.LogIn,
                Password = userBO.Password,

            };
        }
        internal UserBO Convert(User user)
        {
            if (user == null) { return null; }
            return new UserBO()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Address = user.Address,
                Account = user.Account,
                LogIn = user.LogIn,
                Password = user.Password
            };
        }
    }
}
