﻿using ArtGalleryBLL.BusinessObjects;
using ArtGalleryDAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryBLL.Converters
{
    class ArtObjectConverter
    {
        internal ArtObject Convert(ArtObjectBO artObjectBO)
        {
            if (artObjectBO == null) { return null; }
            return new ArtObject()
            {
                Id = artObjectBO.Id,
                Name = artObjectBO.Name,
                Artist = artObjectBO.Artist,
                Type = artObjectBO.Type,
                Description = artObjectBO.Description,
                Time = artObjectBO.Time,
                Price = artObjectBO.Price,
                UserId = artObjectBO.UserId,
                User = new UserConverter().Convert(artObjectBO.User)
                
            };
        }
        internal ArtObjectBO Convert(ArtObject artObject)
        {
            if (artObject == null) { return null; }
            return new ArtObjectBO()
            {
                Id = artObject.Id,
                Name = artObject.Name,
                Artist = artObject.Artist,
                Type = artObject.Type,
                Description = artObject.Description,
                Time = artObject.Time,
                Price = artObject.Price,
                UserId = artObject.UserId,
                User = new UserConverter().Convert(artObject.User)
            };
        }
    }
}

