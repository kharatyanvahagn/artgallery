﻿using Enums;
using System;
using System.Collections.Generic;
using System.Text;
namespace ArtGalleryBLL.BusinessObjects
{
  public  class ArtObjectBO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Artist { get; set; }
        public Types Type { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; }
        public decimal Price { get; set; }
        public int UserId { get; set; }
        public UserBO User { get; set; }

    }
}
