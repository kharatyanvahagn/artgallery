﻿using ArtGalleryDAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ArtGalleryBLL.BusinessObjects
{
    public class UserBO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Account { get; set; }
        [Required]
        [EmailAddress]
        public string LogIn { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        //public List<ArtObject> ArtObjects { get; set; }
    }
}
