﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ArtGalleryBLL;
using ArtGalleryBLL.BusinessObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace ArtGalleryRestAPI.Controllers
{

    [ApiController]
    [Produces("application/json")]
    [Route("api/users")]
    public class UserController : Controller
    {
        private IConfiguration _config;
        public UserController(IConfiguration config)
        {
            _config = config;
        }


        //[HttpGet("/login")]
        [Route("login")]
        public IActionResult Login(string account,string password)
        {
            IActionResult response = Unauthorized();
            var user = bLLFacade.UserService.GetUser(account, password);
            if (user != null)
            {
                var tokenStr = GetTokenString(user);
                response = Ok(new {key = tokenStr});
            }
            return response;


        }
        private string GetTokenString(UserBO user)
        {
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);

            //add claims
            var claims = new List<Claim>();
            claims.Add(new Claim(JwtRegisteredClaimNames.Sub, user.Account));
            claims.Add(new Claim(JwtRegisteredClaimNames.Email, user.LogIn));
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
  
            //create token
            var token = new JwtSecurityToken(
                    issuer: _config["Jwt:Issuer"],
                    audience: _config["Jwt:Issuer"],
                    expires: DateTime.Now.AddHours(1),
                    signingCredentials: signingCredentials,
                    claims: claims
                );

            //return token
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        BLLFacade bLLFacade = new BLLFacade();

        [Authorize]
        // GET : api/User
        public IEnumerable<UserBO> Index()
        { 
         return bLLFacade.UserService.GetAll();
        }

        // GET: api/users/5
        [Authorize]
        [HttpGet("{id}")]
        public UserBO Details(int id)
        {
            return bLLFacade.UserService.Get(id);
        }

        // POST: api/users
        [Authorize]
        [HttpPost]
        public ActionResult Create([FromBody]UserBO userBO)
        {
         /*   if (!ModelState.IsValid)
            {
                return BadRequest("not ok");
            }*/
            return Ok(bLLFacade.UserService.Create(userBO));
        }

        // Put: api/users/5
        [Authorize]
        [HttpPut("{id}")]
          public ActionResult Edit(int id, [FromBody]UserBO userBO)
          {
            if (id != userBO.Id)
            {
                return BadRequest("invalid operation");
            }
            try
            {
               var updatedUser = bLLFacade.UserService.Update(userBO);
                return Ok(updatedUser);
            }
            catch(InvalidOperationException e)
            {
                return StatusCode(404 , e.Message);
            }
           
          }

        // Delete: api/users/5
        [Authorize]
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            bLLFacade.UserService.Delete(id);
        }

    }
}