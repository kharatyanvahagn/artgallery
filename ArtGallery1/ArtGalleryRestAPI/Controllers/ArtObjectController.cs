﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArtGalleryBLL;
using ArtGalleryBLL.BusinessObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ArtGalleryRestAPI.Controllers
{   [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ArtObjectController : ControllerBase
    {
        BLLFacade bLLFacade = new BLLFacade();
        // GET: api/ArtObject
        [HttpGet]
        public IEnumerable<ArtObjectBO> Get()
        {
          return  bLLFacade.ArtObjectService.GetAll();
        }

        // GET: api/ArtObject/5
        [HttpGet("{id}")]
        public ArtObjectBO Get(int id)
        {
            return bLLFacade.ArtObjectService.Get(id);
        }

        // POST: api/ArtObject
        [HttpPost]
        public ActionResult Post([FromBody] ArtObjectBO artObjectBO)
        {
            return Ok( bLLFacade.ArtObjectService.Create(artObjectBO));
        }

        // PUT: api/ArtObject/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] ArtObjectBO artObjectBO)
        {
            if (id != artObjectBO.Id)
            {
                return BadRequest("invalid operation");
            }
            try
            {
                var updatedArt = bLLFacade.ArtObjectService.Update(artObjectBO);
                return Ok(updatedArt);
            }
            catch (InvalidOperationException e)
            {
                return StatusCode(404, e.Message);
            }

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            return Ok(bLLFacade.ArtObjectService.Delete(id));
        }
    }
}
